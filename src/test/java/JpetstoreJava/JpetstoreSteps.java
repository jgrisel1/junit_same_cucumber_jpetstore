package JpetstoreJava;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class JpetstoreSteps {
	
	WebDriver driver;


	@Test
	public void j_accede_a_l_URL_de_JpetStore() {
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addArguments("--no-sandbox");
		chromeOptions.addArguments("--disable-dev-shm-usage");
		chromeOptions.addArguments("--disable-gpu");
		chromeOptions.addArguments("--headless");
		chromeOptions.setBinary("/usr/bin/chromium");
		System.setProperty("webdriver.chrome.whitelistedIps", "");
		driver = new ChromeDriver(chromeOptions);
		//FirefoxOptions firefoxOptions = new FirefoxOptions();
		//firefoxOptions.addArguments("-headless", "-safe-mode");
		//driver = new FirefoxDriver(firefoxOptions);
		//System.setProperty("webdriver.chrome.driver", "src/main/resources/driver/chromedriver.exe");
		//driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.get("https://petstore.octoperf.com/actions/Catalog.action");
	}

	public void je_clique_sur_le_bouton_Login() {
		driver.findElement(By.xpath("//a[contains(.,'Sign In')]")).click();

	}

	public void je_saisis_dans_le_champ_username() throws InterruptedException {
		driver.findElement(By.xpath("//p/input[@name=\"username\"]")).clear();
		driver.findElement(By.xpath("//p/input[@name=\"username\"]")).sendKeys("j2ee");
	}

	public void je_saisis_dans_le_champs_password() {
		driver.findElement(By.xpath("//p/input[@name=\"password\"]")).clear();
		driver.findElement(By.xpath("//p/input[@name=\"password\"]")).sendKeys("j2ee");
	}

	public void je_clique_sur_le_bouton_Login2() {
		driver.findElement(By.xpath("//input[@name=\"signon\"]")).click();	
	}

	public void je_suis_connecte() {
		boolean verif1 = driver.findElement(By.xpath("//a[@href=\"/actions/Account.action?signoff=\"]")).isDisplayed();
		assertTrue(verif1);
	}

	public void Le_message_accueil_suivant_est_lisible() {
		assertEquals("Welcome ABC!",
                driver.findElement(By.xpath("//div[@id='WelcomeContent' and contains(., 'Welcome ABC!')]")).getText());
        assertTrue(
                driver.findElement(By.xpath("//div[@id='MenuContent']/a[contains(@href, 'signoff')]")).isDisplayed());
        driver.quit();

	}
}
